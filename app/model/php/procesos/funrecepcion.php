<?php
header("Content-Type: text/html;charset=utf-8");
$postdata = file_get_contents("php://input");
$request = json_decode($postdata);
$function = $request->function;
$function();

function uiCorrespondencia(){
		require_once('../config/dbcon_local.php');
		global $request;
		$data = json_decode($request->data);
		$myparams['AreaOficinaId'] = $_SESSION['AREA']; //$data->area;
		$myparams['TipoCorrespondenciaDocumentalId'] = $data->tipocorrespondencia;
		$myparams['FechaDocumentoExterno'] = $data->fecha; //date("Y-m-d H:i:s");  //
		$myparams['MensajeriaId'] = $data->empresamensajeria;
		$myparams['CategoriaId'] =  $data->categoria;
		$myparams['UnidadMedidaId'] = $data->unidadmedida;
		$myparams['NumeroGuia'] = $data->numeroguia;
		$myparams['NumeroFolio'] = $data->cantidad;
		$myparams['RemitenteInternoId'] = $data->remitenteinterno;
		$myparams['RemitenteExternoId'] = $data->entidad;
		$myparams['NombreRemitenteExterno'] = $data->nombre;
		$myparams['DestinoId'] = $data->origendest;
		$myparams['DestinatarioExternoId'] = $data->entidaddest;
		$myparams['DestinatarioInternoId'] = $data->remitenteinternodest;
		$myparams['NombreDestinatarioExterno'] = $data->nombredest;
		$myparams['Asunto'] = $data->asunto;
		$myparams['DocumentoUrl'] = '';
		$myparams['Observacion'] = $data->observacion;
		$myparams['UsuarioRegistra'] = $_SESSION['USUARIO'];
		$myparams['strTituloError'] = '';
		$myparams['strMessageError'] = '';
		$myparams['intValorError'] = '';
		$procedure_params = array(	array(&$myparams['AreaOficinaId'],SQLSRV_PARAM_IN),
									array(&$myparams['TipoCorrespondenciaDocumentalId'],SQLSRV_PARAM_IN),
									array(&$myparams['FechaDocumentoExterno'],SQLSRV_PARAM_IN),
									array(&$myparams['MensajeriaId'],SQLSRV_PARAM_IN),
									array(&$myparams['CategoriaId'],SQLSRV_PARAM_IN),
									array(&$myparams['UnidadMedidaId'],SQLSRV_PARAM_IN),
									array(&$myparams['NumeroGuia'],SQLSRV_PARAM_IN),
									array(&$myparams['NumeroFolio'],SQLSRV_PARAM_IN),
									array(&$myparams['RemitenteInternoId'],SQLSRV_PARAM_IN),
									array(&$myparams['RemitenteExternoId'],SQLSRV_PARAM_IN),
									array(&$myparams['NombreRemitenteExterno'],SQLSRV_PARAM_IN),
									array(&$myparams['DestinoId'],SQLSRV_PARAM_IN),
									array(&$myparams['DestinatarioExternoId'],SQLSRV_PARAM_IN),
									array(&$myparams['DestinatarioInternoId'],SQLSRV_PARAM_IN),
									array(&$myparams['NombreDestinatarioExterno'],SQLSRV_PARAM_IN),
									array(&$myparams['Asunto'],SQLSRV_PARAM_IN),
									array(&$myparams['DocumentoUrl'],SQLSRV_PARAM_IN),
									array(&$myparams['Observacion'],SQLSRV_PARAM_IN),
									array(&$myparams['UsuarioRegistra'],SQLSRV_PARAM_IN),
	                            	array(&$myparams['strTituloError'], SQLSRV_PARAM_OUT),
                              		array(&$myparams['strMessageError'], SQLSRV_PARAM_OUT),
                              		array(&$myparams['intValorError'],SQLSRV_PARAM_OUT));
	$consulta = "EXEC sp_insertar_correspondencia
													@AreaOficinaId=?,
													@TipoCorrespondenciaDocumentalId=?,
													@FechaDocumentoExterno=?,
													@MensajeriaId=?,
													@CategoriaId=?,
													@UnidadMedidaId=?,
													@NumeroGuia=?,
													@NumeroFolio=?,
													@RemitenteInternoId=?,
													@RemitenteExternoId=?,
													@NombreRemitenteExterno=?,
													@DestinoId=?,
													@DestinatarioExternoId=?,
													@DestinatarioInternoId=?,
													@NombreDestinatarioExterno=?,
													@Asunto=?,
													@DocumentoUrl=?,
													@Observacion=?,
													@UsuarioRegistra=?,
													@strTituloError =?,
													@strMessageError=?,
													@intValorError=?";
	$stmt = sqlsrv_prepare($con, $consulta, $procedure_params);
	if( !$stmt ) {
		die( print_r( sqlsrv_errors(), true));
	}
	if(sqlsrv_execute($stmt)){
		while($res = sqlsrv_next_result($stmt)){
			// make sure all result sets are stepped through, since the output params may not be set until this happens
		}
		print_r(json_encode(array("Codigo"=>$myparams['intValorError'],"Mensaje"=>$myparams['strMessageError'])));
	}
	else{
		die( print_r( sqlsrv_errors(), true));
	}
	sqlsrv_close( $con );
}

?>
